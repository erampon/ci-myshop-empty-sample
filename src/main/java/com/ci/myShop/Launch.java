package com.ci.myShop;

import com.ci.myShop.model.Item;
import com.ci.myShop.controller.Storage;
import com.ci.myShop.controller.Shop;

class Launch {

public static void main(String[] args) {
		
    Item chocolat= new Item("chocolat",10,5,6);
    Item cafe= new Item("café", 11, 3.5f, 15);

    Storage liste= new Storage();
    liste.addItem(chocolat);
    liste.addItem(cafe);
    
    Shop magasin = new Shop(liste);

    magasin.sell("chocolat");

    Float fondDeCaisse = magasin.getCash();
    System.out.println(fondDeCaisse);

}
}