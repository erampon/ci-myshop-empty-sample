package com.ci.myShop.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ci.myShop.model.Book;
import com.ci.myShop.model.Consumable;
import com.ci.myShop.model.Item;

public class Shop {

    Storage storage;
    float cash;


    public Shop(Storage reserve){
		this.storage = reserve;
        this.cash = 0;
	}

    public Item sell(String name){
        Item produit = storage.getItem(name);
        int nbProduit = produit.getNbrElt();
        if (nbProduit > 0){
            produit.setNbrElt(nbProduit-1);
            this.cash=this.cash+produit.getPrice();
        }
        return produit;
    }    

    public List getAllBook(){
        List liste = new ArrayList(); 
        for(Map.Entry mapentry : storage.itemMap.entrySet()){
			if (mapentry.getValue() instanceof Book){
				liste.add(mapentry.getValue());
			}
       
    }
		return liste;
    }

    public boolean isItemAvailable(String name){
        Item produit = storage.getItem(name);
        int nbProduit = produit.getNbrElt();
        if (nbProduit >=1){
            return true;
            }else {
            return false;
        }

    }

    public int getNbItemInStorage(String name){
        Item produit = storage.getItem(name);
        int nbProduit = produit.getNbrElt();
        return nbProduit;
}

    public int getQuantityPerConsumable(String name){
        Item produit = storage.getItem(name);
        if (produit instanceof Consumable)   {
        	Consumable product_c = (Consumable) produit;
            return product_c.getQuantity();
            }else {
            return 1;
        } 
    }

    public int getAgeForBook(String name){
        Item produit = storage.getItem(name);
        if (produit instanceof Book)   {
        	Book product_b = (Book) produit;
            return product_b.getAge();
            }else {
            return -1;
        } 
    }

    public float getCash() {
		return cash;
	}

    
    
    public Boolean buy(Item it){
        if (cash>=it.getPrice()){
            return true;
        } else {
            return false;
        }
    }
    
}
