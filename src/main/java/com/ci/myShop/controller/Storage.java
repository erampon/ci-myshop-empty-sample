package com.ci.myShop.controller;

import java.util.HashMap;
import java.util.Map;
import com.ci.myShop.model.Item;

public class Storage {
	Map<String , Item > itemMap;
	
	public Storage(){
		this.itemMap=new HashMap<String, Item>();
	}
	
	public void addItem(Item it) {
		this.itemMap.put(it.getName(),it);
	}

	public Item getItem(String name) {
		return itemMap.get(name);
			}

	}
/*
 * public void getItem(Integer id) { return itemMap.get(id); }
 */


