package com.ci.myShop.model;

public class Book extends Item {
		


	public Book(String name, int id, float price, int nbrElt) {
		super(name, id, price, nbrElt);
		// TODO Auto-generated constructor stub
	}

	private int nbPage;
    private String author;
    private String publisher; 
    private int year;
    private int age;

    public int getNbPage() {
		return nbPage;
	}

    public String getAuthor() {
		return author;
	}

    public String getPulisher() {
		return publisher;
	}

    public int getYear() {
		return year;
	}

    public int getAge() {
		return age;
	}

}