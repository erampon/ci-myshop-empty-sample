package com.ci.myShop.model;

import java.util.List;

public class MusicalBook extends Book {
		
	
    public MusicalBook(String name, int id, float price, int nbrElt) {
		super(name, id, price, nbrElt);
		// TODO Auto-generated constructor stub
	}

	private List<String> listOfSound;
    private int lifetime; 
    private int nbrBattery;

    public List<String> getListOfSound() {
		return listOfSound;
	}

    public int getLifetime() {
		return lifetime;
	}   

    public int getNbrBattery() {
		return nbrBattery;
	}   

}