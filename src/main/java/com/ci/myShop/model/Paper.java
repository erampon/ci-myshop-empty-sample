package com.ci.myShop.model;

public class Paper extends Consumable {
		
	
	public Paper(String name, int id, float price, int nbrElt) {
		super(name, id, price, nbrElt);
		// TODO Auto-generated constructor stub
	}

	private String quality;
    private Float weight;

    public String getQuality() {
		return quality;
	}

    public Float getWeight() {
		return weight;
	}

}