package com.ci.myShop.model;

public class Pen extends Consumable {
		
	
	public Pen(String name, int id, float price, int nbrElt) {
		super(name, id, price, nbrElt);
		// TODO Auto-generated constructor stub
	}

	private String color;
    private int durability;

    public String getColor() {
		return color;
	}

    public int getDurability() {
		return durability;
	}

}